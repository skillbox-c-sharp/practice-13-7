﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge1
{
    public delegate void SimpleOperation(bool isDeposit);
    public delegate void BalanceOperation(double value, bool isDeposit);
    public delegate void TransferOperation(Client clientTo, double value, bool isDeposit);
}
